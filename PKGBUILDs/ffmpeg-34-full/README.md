ffmpeg 3.4.4 stable release

**/opt install**

Add `/opt/ffmpeg/3.4-full/lib` to `LD_LIBRARY_PATH` to use.

Still required by some applications.
But don't make this a blocker to use bleeding edge ffmpeg features and
make use of the `/opt` directory and the `LD_LIBRARY_PATH` environment
variable.
