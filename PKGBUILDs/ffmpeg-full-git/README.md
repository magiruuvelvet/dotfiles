[AUR:ffmpeg-full-git](https://aur.archlinux.org/packages/ffmpeg-full-git/)

**system install**

use bleeding edge ffmpeg features by default wherever compatible

current: `4.1+git` (incompatible with `3.4-stable`)
