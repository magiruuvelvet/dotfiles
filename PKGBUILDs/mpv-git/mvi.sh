#!/bin/bash
# mvi ─ MPV Image Viewer
#
# Helper Script
# Opens all image files in the working directory or given directory (max depth = 1)
#
# Usage:
#
#   ./mvi
#     Opens all images in the working directory (sorted) and displays the first image.
#
#   ./mvi file
#     Opens all images in the working directory (sorted) and displays the given file first.
#     Makes use of mpv's --playlist-start option, so you can seek backwards and forwards when using this
#     respecting the image sort order.
#
#   ./mvi dir
#     Opens all images in the given directory (sorted) and displays the first image.
#     When using this method you can't override the display file like the method above.
#
#
# Use the arrow keys to switch between all opened images.
#

export LANGUAGE=ja:ja_JP
export LANG=ja_JP.UTF-8

INVOKE_DIR="$(dirname "$(realpath "$0")")"

images=()

# guessed image list and extensions from `ffmpeg -decoders | grep V`
# since we use mpv which uses ffmpeg its safe to assume all of this
# can be decoded and displayed correctly with mpv
# extensions may be wrong or a bit off for most formats
# side note: below list assumes a 100% non-free and unredistributable ffmpeg build!
function scan_images()
{
    while IFS=  read -r -d $'\0'; do
        # determine file type from `file` command
        # use LC_ALL=C to avoid possible translations
        local type="$(LC_ALL=C file "$REPLY")"

        # check against file extension and than against file type
        if \
        [[ "$REPLY" == *".png" || \
           "$REPLY" == *".bmp" || \
           "$REPLY" == *".tga" || \
           "$REPLY" == *".tpic" || \
           "$REPLY" == *".tif" || \
           "$REPLY" == *".tiff" || \
           "$REPLY" == *".webp" || \
           "$REPLY" == *".apng" || \
           "$REPLY" == *".gif" || \
           "$REPLY" == *".svg" || \
           "$REPLY" == *".jpg" || \
           "$REPLY" == *".jpeg" || \
           "$REPLY" == *".jpe" || \
           "$REPLY" == *".jpgls" || \
           "$REPLY" == *".jpegls" || \
           "$REPLY" == *".jpels" || \
           "$REPLY" == *".jp2" || \
           "$REPLY" == *".jpf" || \
           "$REPLY" == *".jpx" || \
           "$REPLY" == *".mjpeg" || \
           "$REPLY" == *".mjpe" || \
           "$REPLY" == *".mjpg" || \
           "$REPLY" == *".pix" || \
           "$REPLY" == *".anm" || \
           "$REPLY" == *".dpx" || \
           "$REPLY" == *".exr" || \
           "$REPLY" == *".iff" || \
           "$REPLY" == *".pam" || \
           "$REPLY" == *".pbm" || \
           "$REPLY" == *".pcx" || \
           "$REPLY" == *".pgm" || \
           "$REPLY" == *".pgmyuv" || \
           "$REPLY" == *".pictor" || \
           "$REPLY" == *".pixlet" || \
           "$REPLY" == *".ppm" || \
           "$REPLY" == *".psd" || \
           "$REPLY" == *".sgi" || \
           "$REPLY" == *".smvjpeg" || \
           "$REPLY" == *".sun" || \
           "$REPLY" == *".sunrast" || \
           "$REPLY" == *".txd" || \
           "$REPLY" == *".xbm" || \
           "$REPLY" == *".xface" || \
           "$REPLY" == *".xpm" || \
           "$REPLY" == *".xwd" || \
           "$REPLY" == *".qif" || \
           "$REPLY" == *".qtif" || \
           "$REPLY" == *".arw" || \
           "$REPLY" == *".dds" \
        ]] || \
        [[ "$type" == *"PNG image data"* || \
           "$type" == *"GIF image data"* \
        ]]; then
            images+=("$REPLY")
        fi

    # find and sort, above code is the "callback" function
    done < <(LC_ALL=C find "$1" -maxdepth ${2:-1} -print0 -type f | LC_ALL=C sort -z )

    if (( ${#images[@]} == 0 )); then
        echo -e "No image(s) found. Exiting..." >&2
        exit 1
    fi
}

# set opened image as playlist entry point (mpv: playlist-start)
# force C locale for stat command, because stat has translations available which breaks everything
if [[ -d "$1" ]] || [[ -f "$1" ]]; then
    if [[ "$(LC_ALL=C stat -c %F "$1")" == "regular file" ]]; then
        scan_images .
        plist_start=-1
        for i in "${images[@]}"; do
            (( plist_start++ ))
            echo $i  "$(basename "$1")"
            [[ "$i" == *"$(basename "$1")" ]] && break;
        done
    elif [[ "$(LC_ALL=C stat -c %F "$1")" == "directory" ]]; then
        scan_images "$1"
    else
        echo -e "$1: locale problem, please check the output of \`locale\`" >&2
        exit 10
    fi
else
    if [[ ! -z "$1" ]]; then
        echo -e "$1: no such file or not a regular file" >&2
        exit 1
    fi
fi

# scan current directory if empty
(( ${#images[@]} == 0 )) && scan_images .

# set playlist-start if not set already
[[ -z "$plist_start" ]] && plist_start=auto

# debug, set value to true to see script results when started from the GUI file manager
[[ $(false) ]] && \
    kdialog --title "mvi debug" --msgbox \
        "$(echo "PWD: $PWD"; \
        echo "\$1: $1"; \
        echo "Playlist entry point: $plist_start"; \
        echo "\nFound images:"; \
        for i in "${images[@]}"; do echo "$i"; done)"

# execute mpv with the image profile
exec mvi.bin --player-operation-mode=pseudo-gui --profile=image --playlist-start=$plist_start "${images[@]}"
