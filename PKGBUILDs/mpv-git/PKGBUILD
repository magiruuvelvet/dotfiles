
# WARNING: The configure script will automatically enable any optional
# features it finds support for on your system. If you want to avoid
# linking against something you have installed, you'll have to disable
# it in the configure below. The package() script will attempt to
# update the dependencies based on dynamic libraries when packaging.

pkgname=mpv-git
_gitname=mpv
pkgver=46697.g73fe0611b3
pkgrel=1
pkgdesc='Video player based on MPlayer/mplayer2 (git version)'
arch=('i686' 'x86_64' 'armv6h' 'armv7h')
license=('GPL')
url='https://mpv.io'
_undetected_depends=('hicolor-icon-theme')
depends=('ffmpeg' "${_undetected_depends[@]}")
optdepends=('youtube-dl: for --ytdl')
makedepends=('git' 'python-docutils')
provides=('mpv')
conflicts=('mpv')
options=('!emptydirs')
source=('git+https://github.com/mpv-player/mpv'
        'mpv.sh' 'mpv-720p.sh' 'mpv-960p.sh' 'mvi.sh'
        'mpv-streamlink.sh'
        'mpv-720p.desktop' 'mpv-960p.desktop' 'mvi.desktop'
        'find-deps.py')
        #'wscript-fix.patch')
sha256sums=('SKIP'
            'SKIP' 'SKIP' 'SKIP' 'SKIP'
            'SKIP'
            'SKIP' 'SKIP' 'SKIP'
            'ce974e160347202e0dc63f6a7a5a89e52d2cc1db2d000c661fddb9dc1d007c02')
            #'SKIP')

pkgver() {
  cd "$srcdir/$_gitname"
  echo "$(git rev-list --count HEAD).g$(git rev-parse --short HEAD)"
}

prepare() {
  cd "$srcdir/$_gitname"
  ./bootstrap.py
}

build() {
  cd "$srcdir/$_gitname"

  export CC=clang
  export CXX=clang++

  CFLAGS="$CFLAGS -I/usr/include/samba-4.0"
  
  #./waf configure --prefix=/opt/mpv --disable-build-date --enable-libmpv-shared --disable-libarchive --disable-vaapi

  #export PKG_CONFIG_PATH="/opt/mpv/ffmpeg/lib/pkgconfig:$PKG_CONFIG_PATH" # AUR package: ffmpeg-mpv-full-git
  ./waf configure --prefix=/usr \
        CC=$CC CXX=$CXX \
        --confdir=/etc/mpv \
        --disable-build-date \
        --enable-zsh-comp \
        --enable-libmpv-shared \
        --enable-vapoursynth \
        --enable-lua \
        --enable-cplugins \
        \
        --enable-dvdread \
        --enable-dvdnav \
        --enable-cdda \
        --enable-libbluray \
        \
        --enable-libsmbclient \
        --enable-javascript \
        --enable-lcms2 \
        --enable-sndio \
        \
        --enable-libarchive \
        \
        --disable-tv \
        --disable-libv4l2 \
        --disable-tv-v4l2 \
        --disable-dvbin \
        \
        --disable-wayland \
        --disable-wayland-scanner \
        --disable-wayland-protocols

  ./waf -j $(nproc) build
}

package() {
  cd "$srcdir/$_gitname"
  ./waf install --destdir="$pkgdir"

  mv "$pkgdir/usr/bin/mpv" "$pkgdir/usr/bin/mpv.bin"
  cp "$pkgdir/usr/bin/mpv.bin" "$pkgdir/usr/bin/mvi.bin"
  cp "$pkgdir/usr/bin/mpv.bin" "$pkgdir/usr/bin/mpv-streamlink.bin"
  cp "$srcdir/mpv.sh" "$pkgdir/usr/bin/mpv"

  cp "$srcdir/mpv-720p.sh" "$pkgdir/usr/bin/mpv-720p"
  cp "$srcdir/mpv-960p.sh" "$pkgdir/usr/bin/mpv-960p"
  cp "$srcdir/mvi.sh" "$pkgdir/usr/bin/mvi"
  cp "$srcdir/mpv-streamlink.sh" "$pkgdir/usr/bin/mpv-streamlink"

  cp "$srcdir/mpv-720p.desktop" "$pkgdir/usr/share/applications/mpv-720p.desktop"
  cp "$srcdir/mpv-960p.desktop" "$pkgdir/usr/share/applications/mpv-960p.desktop"
  cp "$srcdir/mvi.desktop" "$pkgdir/usr/share/applications/mvi.desktop"

  # Update dependencies automatically based on dynamic libraries
  _detected_depends=($("$srcdir"/find-deps.py "$pkgdir"/usr/{bin/mpv.bin,lib/libmpv.so}))
  echo 'Auto-detected dependencies:'
  echo "${_detected_depends[@]}" | fold -s -w 79 | sed 's/^/ /'
  depends=("${_detected_depends[@]}" "${_undetected_depends[@]}")
}
