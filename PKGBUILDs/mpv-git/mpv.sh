#!/bin/sh
#
# MPV one-instance mode
#

export LANGUAGE=ja:ja_JP
export LANG=ja_JP.UTF-8

# absolute path to mpv binary (pidof)
MPV_BIN="/usr/bin/mpv.bin"

# check if an instance of mpv is running and terminate it
MPV_PID="$(pidof "$MPV_BIN")"
[ -z "$MPV_PID" ] || kill -TERM "$MPV_PID"

# execute new instance
exec "$MPV_BIN" --af-add 'lavfi=graph=[pan=stereo|FL=FL+0.95*FC+BL+0.2*LFE|FR=FR+0.95*FC+BR+0.2*LFE]' --script "/usr/lib/mpv/mpris.so" "$@"
