#!/bin/bash
export LANGUAGE=ja:ja_JP
export LANG=ja_JP.UTF-8
exec /usr/bin/mpv-streamlink.bin --player-operation-mode=pseudo-gui --profile=streamlink "$@"
