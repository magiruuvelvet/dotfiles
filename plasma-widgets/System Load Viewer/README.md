# System Load Viewer

`$XDG_CONFIG_HOME/plasma-org.kde.plasma.desktop-appletsrc`

Containments ID Name: `plugin=org.kde.plasma.systemloadviewer`

**`Containments[Configuration][General]`**

```plain
cacheActivated=true
cacheDirtyColor=202,0,0
cacheWritebackColor=225,225,0
cpuAllActivated=true
cpuIOWaitColor=0,75,103
cpuNiceColor=199,199,0
cpuSysColor=173,0,0
cpuUserColor=0,131,179
memApplicationColor=0,149,203
memBuffersColor=236,236,236
memCachedColor=166,168,166
monitorType=Compact Bar
setColorsManually=true
swapUsedColor=26,93,86
```

*If manual editing of the config file fails see screenshots for graphical configuration.*
