# Useful Links

**→** [Collection of interesting open source projects](https://gist.github.com/magiruuvelvet/4bfe9b998bf1d4937046039bcf682afe) <br>
This list is maintained by me. I collect some interesting projects there which are worth discussing with collegues or just to find them real quick when I forgot their name.
