#####################################################################
# RuboCop Global Default Configuration                              #
#  ~/.rubocop.yml                                                   #
#####################################################################
#
# disable some annoyances globally for any project
#

# use some suggestions from rubocop, but overall I want to use my
# own individual code style for Ruby.
# > indentation with 2 spaces

# doc: https://rubocop.readthedocs.io/

# Enable this when working with the "Ruby on Rails" framework
# << per project setting >>
#Rails:
#  Enabled: true

AllCops:
  TargetRubyVersion: 2.6

# just no
Bundler/OrderedGems:
  Enabled: false

# why does this even exist?
# what about non-english code comments???
Style/AsciiComments:
  Enabled: false

# allow CJK language identifiers
Naming/AsciiIdentifiers:
  Enabled: false

# disable warnings about single and double quotes
# BECAUSE:
#  > single quotes -> single character constant
#  > double quotes -> string
StringLiterals:
  Enabled: false

# Don't annoy about string interpolations (see above option)
Style/UnneededInterpolation:
  Enabled: false
Style/StringLiteralsInInterpolation:
  Enabled: false

# Always use operators for comparisons, rather than this cryptic and unreadable
#   #zero?, #positive? and #negative? methods
Style/NumericPredicate:
  Enabled: true
  EnforcedStyle: comparison

# [rdoc]
# use an external tools to check and validate documentation strings
# don't annoy about possible missing docstrings during development
Style/Documentation:
  Enabled: false

# https://www.rubydoc.info/gems/rubocop/RuboCop/Cop/Style/FrozenStringLiteralComment
Style/FrozenStringLiteralComment:
  Enabled: false

# Don't annoy about DateTime usage
Style/DateTime:
  Enabled: false

# Don't annoy about paths
Style/ExpandPathArguments:
  Enabled: false

# Don't annoy about trailing commas (that's how I do it in C++ too)
Style/TrailingCommaInHashLiteral:
  Enabled: false
Style/TrailingCommaInArrayLiteral:
  Enabled: false
Style/TrailingCommaInArguments:
  Enabled: false

# Don't annoy about "unless-else" stuff
# in general, don't use the "unless" keyword at all, it looks ugly and makes no sense
# use "if !" instead as its better to understand and read
Style/UnlessElse:
  Enabled: false
Style/IfUnlessModifier:
  Enabled: false

Style/IfInsideElse:
  Enabled: false

# Don't annoy about guard clauses
Style/GuardClause:
  Enabled: false
Layout/EmptyLineAfterGuardClause:
  Enabled: false

# Don't annoy about multi-line ternary operators
Style/MultilineTernaryOperator:
  Enabled: false
Style/NestedTernaryOperator:
  Enabled: false

# Don't annoy about parentheses around ternary operators
Style/TernaryParentheses:
  Enabled: false

# Don't annoy about parentheses when parameters/arguments are empty
Style/DefWithParentheses:
  Enabled: false

# Don't annoy about negated if condition
# never use "unless" at all, use "if !" as in every other programming language
Style/NegatedIf:
  Enabled: false

# Don't annoy about %i and %I
Style/SymbolArray:
  Enabled: false

# Avoid using deprecated methods, which may be removed without notice in future Ruby versions
Style/PreferredHashMethods:
  Enabled: true

# Don't annoy about possible redundant "self"
Style/RedundantSelf:
  Enabled: false

# Don't annoy about parentheses around method calls
# I want to use as less as possible parentheses in Ruby, but please Rubocop, don't annoy me about it
# when it improves code readability at some random places
Style/RedundantParentheses:
  Enabled: false

# Don't annoy about parentheses around conditions
# ...
Style/ParenthesesAroundCondition:
  Enabled: false

# Make sure to use the new hash syntax, don't care about Ruby 1.9 and older
Style/HashSyntax:
  Enabled: true

# Don't annoy about parentheses when arguments are empty
Style/MethodCallWithoutArgsParentheses:
  Enabled: false

# Don't annoy about single line methods
Style/SingleLineMethods:
  Enabled: false

# existing code: don't annoy about one-line if/elsif...
# don't do this for myself tho :) as it's really shitty to read and understand
Style/OneLineConditional:
  Enabled: false

# What about static module/class variables??? (@@)
Style/ClassVars:
  Enabled: false

# Don't annoy on static method calls (@@)
# "::" masterrace /s
Style/ColonMethodCall:
  Enabled: false

# This seems a bit weird in Ruby; don't annoy about explicit return statements for better readability
# also I don't want values to leak, so in my code you will see returns everywhere, specially for
# "void"-procedures which always should return "nil" instead of leaking the last value!!!
Style/RedundantReturn:
  Enabled: false

# Don't annoy about the compact style
Style/ClassAndModuleChildren:
  Enabled: false

# Don't warn about semicolons (mostly for some nice one-liner)
# Don't actually use semicolons as line terminators as Ruby doesn't require them
Style/Semicolon:
  Enabled: false

# Remove nonsense warnings about using `next` to skip iteration
Style/Next:
  Enabled: false

# Don't annoy about the usage of begin blocks
Style/RedundantBegin:
  Enabled: false
Layout/SpaceBeforeBlockBraces:
  Enabled: false

# In rare cases it sometimes makes sense to universally catch-all exceptions of no specific type
# so don't annoy about this when this happens
Style/RescueStandardError:
  Enabled: false

# Rather useless warning when working with existing code, use this for new code though or when rewriting code
# looks obvious enough without the help of rubocop
Style/ConditionalAssignment:
  Enabled: false

# Don't annoy about multiple comparisons
Style/MultipleComparison:
  Enabled: false

# percent literal delimiters
Style/PercentLiteralDelimiters:
  Enabled: false

# Use a C++ like indentation level for access modifiers
Layout/AccessModifierIndentation:
  Enabled: false

# Don't annoy about "missing" space after '#'
# helps to distinguish from commented out (one-line) code and documentation
Layout/LeadingCommentSpace:
  Enabled: false

# Don't annoy about comment indentation
Layout/CommentIndentation:
  Enabled: false

# Don't annoy about empty lines around multi line function calls
Layout/EmptyLinesAroundArguments:
  Enabled: false

# Don't annoy about array indentation (custom indentation and alignment)
Layout/IndentFirstArrayElement:
  Enabled: false

# Don't annoy about this
Layout/IndentFirstArgument:
  Enabled: false

# WTF!? this is useful for "END"-type of comments to keep things readable (long function bodies and methods)
Style/CommentedKeyword:
  Enabled: false

# Don't annoy about inconsistent indentation
Layout/IndentationConsistency:
  Enabled: false

# Don't annoy about other indentation stuff
Layout/IndentFirstHashElement:
  Enabled: false

# Use explicit Hash for everything to distinguish it from other arguments
Style/BracesAroundHashParameters:
  Enabled: false

# Don't annoy about spacing
Layout/EmptyLineBetweenDefs:
  Enabled: false
Layout/ExtraSpacing:
  Enabled: false
Layout/SpaceInsideParens:
  Enabled: false

# Don't annoy about "missing" spaces inside hash literals
Layout/SpaceInsideHashLiteralBraces:
  Enabled: false

# Don't annoy about "missing" spaces inside blocks
Layout/SpaceInsideBlockBraces:
  Enabled: false

# Don't annoy about multi-line hash alignments
Layout/AlignHash:
  Enabled: false
Layout/MultilineHashBraceLayout:
  Enabled: false

# access modifiers
Layout/EmptyLinesAroundAccessModifier:
  Enabled: false

# multi-line method call indentation
Layout/MultilineMethodCallIndentation:
  Enabled: false
Layout/DotPosition:
  Enabled: false

Layout/MultilineMethodCallBraceLayout:
  Enabled: false

# Don't annoy about parameter alignment
Layout/AlignParameters:
  Enabled: false

# Don't annoy about parentheses at same column, align with function/method name instead
Layout/ClosingParenthesisIndentation:
  Enabled: false

# Don't annoy about multi-line block layouts
Layout/MultilineBlockLayout:
  Enabled: false
Style/BlockDelimiters:
  Enabled: false
Layout/BlockEndNewline:
  Enabled: false

# Don't annoy about multi-line block chains
Style/MultilineBlockChain:
  Enabled: false

# Don't annoy about exceptions
Style/RaiseArgs:
  Enabled: false
Style/RescueModifier:
  Enabled: false

# Don't annoy about "missing" spaces around default parameters
Layout/SpaceAroundEqualsInParameterDefault:
  Enabled: false

# Don't annoy about more than one space around operators, some alignments sometimes require this
Layout/SpaceAroundOperators:
  Enabled: false

# Indent "when" one level deeper than "case" for better readability.
# The default rubocop config is really weird here :/
Layout/CaseIndentation:
  Enabled: false

# Don't annoy about extra empty lines
Layout/EmptyLines:
  Enabled: false
Layout/LeadingBlankLines:
  Enabled: false
Layout/EmptyLinesAroundMethodBody:
  Enabled: false
Layout/EmptyLinesAroundModuleBody:
  Enabled: false
Layout/EmptyLinesAroundBlockBody:
  Enabled: false
Layout/EmptyLinesAroundClassBody:
  Enabled: false
Layout/EmptyLinesAroundBeginBody:
  Enabled: false
Layout/EmptyLinesAroundExceptionHandlingKeywords:
  Enabled: false

# Also get rid of this shitty style decision
# Empty methods should not exist in the first place, except in some "Ruby on Rails" specific cases
# still put the "end" keyword on the next line!!
Style/EmptyMethod:
  Enabled: false

# Don't annoy about shadowed variable names
# I even do this in C++
Lint/ShadowingOuterLocalVariable:
  Enabled: false

# RubyMine warns about this anyway, so silence the redundant warning from rubocop
Lint/HandleExceptions:
  Enabled: false

# Silence this one
Lint/LiteralAsCondition:
  Enabled: false

# Don't annoy about line lengths
Metrics/LineLength:
  Enabled: false

# Don't annoy about "Method/Class/Block/Module has too many lines"! Why tho...
Metrics/MethodLength:
  Enabled: false
Metrics/ClassLength:
  Enabled: false
Metrics/BlockLength:
  Enabled: false
Metrics/ModuleLength:
  Enabled: false

Metrics/BlockNesting:
  Enabled: false

# complexity options
Metrics/CyclomaticComplexity:
  Enabled: false
Metrics/PerceivedComplexity:
  Enabled: false

# ???
Metrics/AbcSize:
  Enabled: false

# casing of method names
Naming/MethodName:
  Enabled: false

# casing of variable names
Naming/VariableName:
  Enabled: false
Naming/VariableNumber:
  Enabled: false

# is_something? vs something?
Naming/PredicateName:
  Enabled: false

# Don't annoy about short parameter names in some special cases
Naming/UncommunicativeMethodParamName:
  Enabled: false

# What even is this???? I name my functions and methods as I want them
# "Do not prefix reader method names with ..."
Naming/AccessorMethodName:
  Enabled: false

# forward declaration always makes sense and should be required anyway (as C++ dev)
Lint/UselessAssignment:
  Enabled: false

Style/NumericLiterals:
  Enabled: false

# don't annoy about splat expansions like *[]
Lint/UnneededSplatExpansion:
  Enabled: false

# don't
Style/WordArray:
  Enabled: false

# don't complain about long parameter lists
Metrics/ParameterLists:
  Enabled: false
