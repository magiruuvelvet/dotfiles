# Private Font Mirror (GitHub)

Global install (without conflicting with repositories): `/usr/share/fonts/custom`

**Handy alias to update the font cache**
```sh
alias update-fontcache='sudo fc-cache -fv'
```

## Hint

Font packages are encrypted due to copyright and licensing.
