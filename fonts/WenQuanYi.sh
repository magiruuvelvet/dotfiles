#!/bin/bash
pacman -S wqy-microhei wqy-microhei-lite wqy-zenhei

### default Monospace font for everything: "WenQuanYi Micro Hei Mono"
# KWrite/Kate, Terminal/Konsole, ...

## wqy-microhei
#/usr/share/fonts/wenquanyi/wqy-microhei/wqy-microhei.ttc: WenQuanYi Micro Hei,文泉驛微米黑,文泉驿微米黑:style=Regular
#/usr/share/fonts/wenquanyi/wqy-microhei/wqy-microhei.ttc: WenQuanYi Micro Hei Mono,文泉驛等寬微米黑,文泉驿等宽微米黑:style=Regular

## wqy-microhei-lite
#/usr/share/fonts/wenquanyi/wqy-microhei-lite/wqy-microhei-lite.ttc: WenQuanYi Micro Hei Light,文泉驛微米黑,文泉驿微米黑:style=Light
#/usr/share/fonts/wenquanyi/wqy-microhei-lite/wqy-microhei-lite.ttc: WenQuanYi Micro Hei Mono Light,文泉驛等寬微米黑,文泉驿等宽微米黑:style=Light

## wqy-zenhei
#/usr/share/fonts/wenquanyi/wqy-zenhei/wqy-zenhei.ttc: WenQuanYi Zen Hei Mono,文泉驛等寬正黑,文泉驿等宽正黑:style=Regular
#/usr/share/fonts/wenquanyi/wqy-zenhei/wqy-zenhei.ttc: WenQuanYi Zen Hei Sharp,文泉驛點陣正黑,文泉驿点阵正黑:style=Regular
#/usr/share/fonts/wenquanyi/wqy-zenhei/wqy-zenhei.ttc: WenQuanYi Zen Hei,文泉驛正黑,文泉驿正黑:style=Regular
